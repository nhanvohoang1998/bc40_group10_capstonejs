//show product lists
export function showProducts(productList) {
    let contentStr = ``
    productList.forEach((item) => {
        let content = `
                        <div class="card mb-3">
                            <div class="img-container">
                                <img class="img-fluid max-width" src="${item.img}">
                            </div>
                            <div class="details">
                                <div class="wbproductdes">
                                    <p class="details__name">${item.name}</p>
                                    
                                    <p>${item.backCamera}</p>
                                    <p>${item.frontCamera}</p>
                                    <p>${item.desc}</p>
                                    <p>${item.type}</p>
                                    <div class="price row justify-content-between">
                                    <span>${item.price}$</span>
                                    <button class="btn btn-warning" onclick="addItem(${item.id})">Add</button></div>
                                </div>
                            </div>
                        </div>
                    `
        contentStr += content
    })
    document.querySelector(".tab-content").innerHTML = contentStr
}

//render Cart
export function renderCart(cart){
    let contentStr=``
    let countItem = 0
    let sumPrice= 0
    cart.forEach((item)=>{
        let content = `<tr>
                        <td><img style="width:100px; height:100%" src="${item.product.img}"></td>
                        <td>${item.product.name}</td>
                        <td>${item.product.price}$</td>
                        <td>
                            <div class="d-flex">
                                <button class="btnquantity" onclick="upDownQuantity(${item.product.id},${true})"><i class="fa fa-chevron-left"></i></button>
                                <span class="px-1">${item.quantity}</span>
                                <button class="btnquantity" onclick="upDownQuantity(${item.product.id},${false})"><i class="fa fa-chevron-right"></i></button>
                            </div>
                        </td>
                        <td>
                            <i class="fa fa-trash" onclick="removeItems(${item.product.id})"></i>
                        </td>
                    </tr>`
        contentStr+=content
        countItem+=item.quantity
        sumPrice+= item.product.price*item.quantity
    })
    document.getElementById("countItem").innerHTML = `${countItem}`
    contentStr+=`<tr>
                    <td colspan="3" class="text-right">Total:${sumPrice}$</td>
                    <td colspan="2" class="text-right"><button class="btn btn-warning" onclick="purchase()">Purchase</button></td>
                </tr>`
    return contentStr;
}

export function addToLocalStorage(cart){
    let data = JSON.stringify(cart)
    localStorage.setItem("CART", data)
}

