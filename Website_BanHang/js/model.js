export function Product(id, name,
    price,
    screen,
    blackCamera,
    frontCamera,
    img,
    desc,
    type) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.screen = screen;
    this.backCamera = blackCamera;
    this.frontCamera = frontCamera;
    this.img = img;
    this.desc = desc;
    this.type = type;
}

export function cartItem(product, quantity) {
    this.product = { id: product.id, img:product.img, price: product.price, name: product.name };
    this.quantity = quantity;
}