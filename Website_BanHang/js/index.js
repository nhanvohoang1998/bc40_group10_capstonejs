import { addToLocalStorage, renderCart, showProducts } from "./controller.js"
import { cartItem } from "./model.js"

const ULR_BASE = `https://63d9dff9b28a3148f67ab930.mockapi.io/phone`
let productLists = []
let cart = []

// get data from local storage to render 
let data = localStorage.getItem("CART")
if (data != null) {
    cart = JSON.parse(data)
    document.getElementById("cartItems").innerHTML = renderCart(cart);
}

//fetch phone
function fecthPhone() {
    axios({
        url: ULR_BASE,
        method: "GET"
    })
        .then((res) => {
            console.log(res.data)
            productLists = res.data
            showProducts(productLists)

        })
        .catch((err) => {
            console.log("🚀 ~ file: index.js:11 ~ fecthPhone ~ err", err)
        })
}
fecthPhone()

//filter type of phone
function filterPhone() {
    axios({
        url: ULR_BASE,
        method: "GET"
    })
        .then((res) => {
            productLists = res.data
            let result = null
            let chooseOption = document.getElementById("inputState").value * 1;
            if (chooseOption === 2) {
                result = productLists.filter((item) => { return item.type === "iphone" })
            } else if (chooseOption === 3) {
                result = productLists.filter((item) => { return item.type === "Samsung" })
            } else {
                result = productLists
            }
            showProducts(result)

        })
        .catch((err) => {
            console.log("🚀 ~ file: index.js:11 ~ fecthPhone ~ err", err)
        })

}
document.getElementById("inputState").addEventListener("change", filterPhone)

//add item into cart
function addItem(id) {
    let product = productLists.find((item) => { return item.id == id })
    let cartItems = new cartItem(product, 1)
    if (cart.length == 0) {
        cart.push(cartItems)
    } else {
        let indexItem = cart.findIndex((item) => { return item.product.id == cartItems.product.id })
        if (indexItem == -1) {
            cart.push(cartItems)
        } else {
            cart[indexItem].quantity += 1;
        }
    }
    addToLocalStorage(cart);
    document.getElementById("cartItems").innerHTML = renderCart(cart);

}
//remove item into cart
function removeItems(id) {
    let indexItem = cart.findIndex((item) => { return item.product.id == id })
    if (indexItem == -1) {
        console.log("báo lỗi")
    } else {
        cart.splice(indexItem, 1)
    }
    addToLocalStorage(cart);
    document.getElementById("cartItems").innerHTML = renderCart(cart);
}

//up down quantity
function upDownQuantity(id, isCheck) {
    let indexItem = cart.findIndex((item) => { return item.product.id == id })
    if (isCheck) {
        cart[indexItem].quantity -= 1;
        if (cart[indexItem].quantity == 0) {
            removeItems(id)
        }
    } else {
        cart[indexItem].quantity += 1;
    }
    addToLocalStorage(cart);
    document.getElementById("cartItems").innerHTML = renderCart(cart);
}

//purchase
function purchase() {
    cart = []
    addToLocalStorage(cart);
    document.getElementById("cartItems").innerHTML = renderCart(cart);
}
window.purchase = purchase
window.upDownQuantity = upDownQuantity
window.removeItems = removeItems
window.addItem = addItem
