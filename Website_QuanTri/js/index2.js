import { layThongTinForm, onSuccess, renderPhone, showThongTinLenForm } from "./controller2.js";
import { KtChoTrong, KtSo } from "./validate.js";

const ULR_BASE = `https://63d9dff9b28a3148f67ab930.mockapi.io/phone`
document.getElementById("themSp").addEventListener("click", () => {
    document.getElementById("IdSP").value = " "; 
    document.getElementById("TenSP").value = " ";
    document.getElementById("GiaSP").value = " ";
    document.getElementById("HinhSP").value = " ";
    document.getElementById("MoTa").value = " ";
})
let fecthPhone = () => {
    axios({
        url: ULR_BASE,
        method: "GET",
    }).then((res) => {
        console.log(res);
        renderPhone(res.data)
        let i= res.data;
        console.log("🚀 ~ file: index2.js:27 ~ i", i)
    })
        .catch((err) => {
            console.log(err);
        });
}

fecthPhone();


// thêm
document.getElementById("add").addEventListener("click", () => {

    let phone = layThongTinForm();
    
    let isValidate =  KtSo(phone.id,"tbId") & 
    KtChoTrong(phone.name,"tbName") &
    KtSo(phone.price,"tbGia") &
    KtChoTrong(phone.screen,"tbScreen") &
    KtChoTrong(phone.backCamera,"tbBcamera") &
    KtChoTrong(phone.frontCamera,"tbFcamera") &
    KtChoTrong(phone.img,"tbHinh") &
    KtChoTrong(phone.desc,"tbDesc") &
    KtChoTrong(phone.type,"tbType") 
    ;
    
    if(isValidate){
        axios({
            url: ULR_BASE,
            method: "POST",
            data: phone,
        }).then((res) => {
            console.log(res);
            fecthPhone();
            document.querySelector("#myModal .close").click()
            onSuccess("Thêm Sản Phẩm Thành Công")
        })
            .catch((err) => {
                console.log(err);
            }); 
    }
    
})

//  xóa
let xoaSP = (id) => {
    axios({
        url: `${ULR_BASE}/${id}`,
        method: "DELETE",
    }).then((res) => {
        console.log(res);
        fecthPhone();
        onSuccess("Xóa Sản Phẩm Thành Công")
    })
        .catch((err) => {
            console.log(err);
        });
}
window.xoaSP = xoaSP;

//  sửa
let fix = (id) => {
    axios({
        url: `${ULR_BASE}/${id}`,
        method: "GET"
    }).then((res) => {
        console.log(res);
        showThongTinLenForm(res.data);

    })
        .catch((err) => {
            console.log(err);
        });
}

window.fix = fix;

// cập nhật 
document.getElementById("update").addEventListener("click", () => {
    let phone = layThongTinForm();
    let isValidate =  KtSo(phone.id,"tbId") & 
    KtChoTrong(phone.name,"tbName") &
    KtSo(phone.price,"tbGia") &
    KtChoTrong(phone.screen,"tbScreen") &
    KtChoTrong(phone.backCamera,"tbBcamera") &
    KtChoTrong(phone.frontCamera,"tbFcamera") &
    KtChoTrong(phone.img,"tbHinh") &
    KtChoTrong(phone.desc,"tbDesc") &
    KtChoTrong(phone.type,"tbType") 
    ;
    if(isValidate){
        axios({
            url: `${ULR_BASE}/${phone.id}`,
            method: "PUT",
            data: phone,
        }).then((res) => {
            console.log(res);
            fecthPhone();
            document.querySelector("#myModal .close").click();
            onSuccess("Cập Nhật Sản Phẩm Thành Công")
        })
            .catch((err) => {
                console.log(err);
            });
    }

    
})


