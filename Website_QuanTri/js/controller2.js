import { Product } from "./model2.js";

export let renderPhone = (phoneList) => {
    let content = "";
    phoneList.forEach((item) => {
        let contentTr =/*html*/ `
            <tr>
            <td>${item.id}</td>
            <td>${item.name}</td>
            <td>${item.price}</td>
            <td><img class="img__phone img-fluid max-width" src="${item.img}"></td>
            <td>${item.screen}</td>
            <td>${item.backCamera}</td>
            <td>${item.frontCamera}</td>
            <td>${item.type}</td>
            <td>${item.desc}</td>
            <td><button onclick="xoaSP(${item.id})" class="btn btn-danger">Xóa</button>
            <button onclick="fix(${item.id})" class="btn btn-success mt-2" data-toggle="modal" data-target="#myModal">Sửa</button>
            </td>
            
            </tr>
         `
        content += contentTr;
    });
    document.getElementById("tbDSSP").innerHTML = content;
}

export let layThongTinForm = () => {
    let id = document.getElementById("IdSP").value;
    let name = document.getElementById("TenSP").value;
    let price = document.getElementById("GiaSP").value;
    let img = document.getElementById("HinhSP").value;
    let desc = document.getElementById("MoTa").value;
    let screen= document.getElementById("screenSP").value ;
    let backCamera = document.getElementById("bCameraSP").value ;
    let frontCamera = document.getElementById("fcameraSP").value ;
    let type = document.getElementById("LoaiSP").value ;

    let phone = new Product (id,name,price,screen,backCamera,frontCamera,img,desc,type)
    return phone;
}

export let showThongTinLenForm = (phone) => {
    document.getElementById("IdSP").value= phone.id;
    document.getElementById("TenSP").value = phone.name;
    document.getElementById("GiaSP").value = phone.price;
    document.getElementById("HinhSP").value = phone.img;
    document.getElementById("screenSP").value = phone.screen;
    document.getElementById("bCameraSP").value = phone.backCamera;
    document.getElementById("fcameraSP").value = phone.frontCamera;
    document.getElementById("LoaiSP").value = phone.type;
    document.getElementById("MoTa").value = phone.desc;
}

export let onSuccess =(message) => { 
    Toastify({
      text: message,
      className: "info",
      offset: {
        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
        y: 10, // vertical axis - can be a number or a string indicating unity. eg: '2em'
      },
   }).showToast();
  }

