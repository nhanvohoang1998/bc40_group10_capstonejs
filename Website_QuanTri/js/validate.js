export let KtChoTrong= (value,idErr) => { 
    let reg = /^\S/;
    let isSpace = reg.test(value);
    if(isSpace){
        document.getElementById(idErr).innerHTML=" ";
        return true;
    }else{
        document.getElementById(idErr).innerHTML="Không Được Để Trống";
        return false;
    }
 }

 export let KtSo = (value,idErr) => { 
    let reg = /^[0-9]+$/;
    let isSo = reg.test(value);
    if(isSo){
        document.getElementById(idErr).innerHTML=" ";
        return true
    }else{
        document.getElementById(idErr).innerHTML="Giá Trị Phải Là Số Và Không Được Để Trống";
        return false
    }
  }